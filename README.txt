------------------------------------------------------------------------------
  usenet quotes filter Readme
  http://drupal.org/project/usenet_quotes

  Author: 
  David Herminghaus (doitDave)
  www.david-herminghaus.de (http://drupal.org/user/833794)
------------------------------------------------------------------------------

Contents:
=========
1. ABOUT
2. INSTALLATION/SETUP

1. ABOUT
========
This module transforms usenet style quotes into custom HTML or BB code quotes.

E.g. the following:

Richard says:
>>Level 2
>>>Level 3
>Level 1
But I don't agree.

transforms into this:

Richard says:
<blockquote><blockquote>Level 2<blockquote>Level 3
</blockquote></blockquote>Level 1</blockquote>
But I don't agree.

2. INSTALLATION/SETUP
=====================

1) Install this module as usual. 
2) Edit your input formats in admin/settings/filters and find the new
   filter listed there. If you do not want to use the default <blockquote />
   tags as replacements, configure whatever you like to for each of your
   input formats.
